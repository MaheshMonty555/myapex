@isTest
public class Demo1 {

    @isTest
    static void accountTest()
    {
        Account acc = new Account(Name='TestAcct',Industry='Education',Rating='Hot',Phone='9988776655');
        insert acc;
    }
     @isTest
    static void contactTest()
    {
        Account acc = new Account(Name='TestAcct',Industry='Education',Rating='Hot',Phone='9988776655');
        insert acc;
        Contact con = new Contact(FirstName='Mahesh',LastName='Monty',Email='maheshmonty555@gmail.com',
                                  Phone='9000000009',AccountId = acc.Id);
        insert con;
    }
     @isTest
    static void opportunityTest()
    {
        Account acc = new Account(Name='TestAcct',Industry='Education',Rating='Hot',Phone='9988776655');
        insert acc;
        Opportunity opp = new Opportunity(Name='Cloud Project',AccountId = acc.Id,
                                          Amount=989.35,StageName='Closed Won',
                                          CloseDate=System.today().addMonths(8));
        insert opp;
    }
}